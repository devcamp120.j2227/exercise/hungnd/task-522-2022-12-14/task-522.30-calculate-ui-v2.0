import { Container, Grid } from "@mui/material";
import FormCalculate from "./components/CalculateForm";
import DisplayNumber from "./components/displayNumber";
import ButtonStyledProps from "./components/ButtonWithProps";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
function App() {
  return (
    <Container disableGutters xs={{padding:0}}>
      <FormCalculate>
        <Grid>
          <Grid>
            <DisplayNumber>
            </DisplayNumber>
          </Grid>
          <Grid container spacing={1}>
            <Grid item xs={6} >
              <ButtonStyledProps bg="rgb(209,212,218)">
                  DEL
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3}>
              <ButtonStyledProps bg="rgb(209,212,218)">
                <ArrowBackIcon fontSize="small"/>
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(185,215,213)">
                /
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                  7
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                  8
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3}>
              <ButtonStyledProps bg="rgb(229,228,234)"color="rgb(100,175,164)">
                9
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(185,215,213)">
                *
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                  4
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                  5
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3}>
              <ButtonStyledProps bg="rgb(229,228,234)"color="rgb(100,175,164)">
                6
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(185,215,213)">
                -
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                1
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                2
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3}>
              <ButtonStyledProps bg="rgb(229,228,234)"color="rgb(100,175,164)">
                3
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(185,215,213)">
                +
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3} >
              <ButtonStyledProps bg="rgb(229,228,234)" color="rgb(100,175,164)">
                0
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={3}>
              <ButtonStyledProps bg="rgb(229,228,234)"color="rgb(100,175,164)">
                ,
              </ButtonStyledProps>
            </Grid>
            <Grid item xs={6} >
              <ButtonStyledProps bg="rgb(100,175,164)" color="white">
                  =
              </ButtonStyledProps>
            </Grid>
          </Grid>
        </Grid>
      </FormCalculate>
    </Container>
  );
}

export default App;
