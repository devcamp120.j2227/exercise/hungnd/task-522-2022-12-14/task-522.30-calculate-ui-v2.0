import styled from "styled-components";

const ButtonStyledProps = styled.button`
    background-color: ${props => props.bg};
    color: ${props => props.color};
    border: none;
    border-radius: 5px;
    font-size: 1.25rem;
    width: 100%;
    height: 80px;
    padding: 0px;
`;

export default ButtonStyledProps;